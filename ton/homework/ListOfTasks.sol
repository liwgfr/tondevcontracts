//3.2. "Список задач"
//Структура (см лекцию по типам данных)
//- название дела
//- время добавления (см helloWorld)
//- флаг выполненности дела (bool)Структуру размещаем в сопоставление int8 => struct (см лекцию по типам данных)должны быть доступны опции:
//- добавить задачу (должен в сопоставление заполняться последовательный целочисленный ключ)
//- получить количество открытых задач (возвращает число)
//- получить список задач
//- получить описание задачи по ключу
//- удалить задачу по ключу
//- отметить задачу как выполненную по ключу

pragma ton -solidity >= 0.35.0;
pragma AbiHeader expire;

contract ListOfTasks {
    int8 idx = 0;

    struct task {
        string name;
        uint timestamp;
        bool isDone;
    }

    mapping(int8 => task) taskAr;

    constructor() public {
        // Check that contract's public key is set
        require(tvm.pubkey() != 0, 101);
        // Check that message has signature (msg.pubkey() is not zero) and
        // message is signed with the owner's private key
        require(msg.pubkey() == tvm.pubkey(), 102);
        // The current smart contract agrees to buy some gas to finish the
        // current transaction. This actions required to process external
        // messages, which bring no value (henceno gas) with themselves.
        tvm.accept();

    }
    function addTask(string name, bool isDone) public returns (task t) {
        task t = task(name, now, isDone);
        taskAr[idx] = t;
        idx++;
        tvm.accept();
        return t;
    }

    function getActive() public returns (int8 cnt) {
        int8 counter = 0;

        for (int8 i = 0; i < idx; i++) {
            if (bytes(taskAr[i].name).length != 0 && !taskAr[i].isDone) {
                counter++;
            }
        }
        tvm.accept();
        return counter;
    }

    function getList() public returns (mapping(int8 => task) ar) {
        tvm.accept();
        return taskAr;
    }

    function getTask(int8 key) public returns (task t) {
        tvm.accept();
        return taskAr[key];
    }

    function remTask(int8 key) public {
        tvm.accept();
        delete taskAr[key];
    }

    function setDone(int8 key) public {
        tvm.accept();
        taskAr[key].isDone = true;
    }

}
