pragma ton-solidity >= 0.35.0;
pragma AbiHeader expire;

/// @title Simple wallet
/// @author Tonlabs
/*
1. Написать смарт-контракт Wallet.
пример есть здесь: https://github.com/tonlabs/samples/blob/master/solidity/10_Wallet.sol
Но расширить функционал следующими методами.
- Отправить без оплаты комиссии за свой счет
- Отправить с оплатой комисси за свой счет
- Отправить все деньги и уничтожить кошелек
В лекции все эти вещи были показаны, но интерфейс был не дружественен пользователю - где-то приходилось лишние параметры вносить. Где-то приходилось вбивать номера флагов, которые клиент по идее может не знать.
*/
contract Wallet {
    /*
     Exception codes:
      100 - message sender is not a wallet owner.
      101 - invalid transfer value.
     */

    /// @dev Contract constructor.
    constructor() public {
        // check that contract's public key is set
        require(tvm.pubkey() != 0, 101);
        // Check that message has signature (msg.pubkey() is not zero) and message is signed with the owner's private key
        require(msg.pubkey() == tvm.pubkey(), 102);
        tvm.accept();
    }


    // Modifier that allows function to accept external call only if it was signed
    // with contract owner's public key.
    modifier checkOwnerAndAccept {
        // Check that inbound message was signed with owner's public key.
        // Runtime function that obtains sender's public key.
        require(msg.pubkey() == tvm.pubkey(), 100);

        // Runtime function that allows contract to process inbound messages spending
        // its own resources (it's necessary if contract should process all inbound messages,
        // not only those that carry value with them).
        tvm.accept();
        _;
    }

    /// @dev Allows to transfer tons to the destination account.
    /// @param dest Transfer target address.
    /// @param value Nanotons value to transfer.
    /// @param bounce Flag that enables bounce message in case of target contract error.
    function sendTransactionNoFeeWithReturn(address dest, bool bounce, uint128 value, uint16 flag) public pure checkOwnerAndAccept {
        // Runtime function that allows to make a transfer with arbitrary settings.
        dest.transfer(value, true, 0);
    }
    function sendTransactionWithFeeWithReturn(address dest, bool bounce, uint128 value, uint16 flag) public pure checkOwnerAndAccept {
        // Runtime function that allows to make a transfer with arbitrary settings.
        dest.transfer(value, true, 1);
    }
    function sendTransactionWithFeeNoReturn(address dest, bool bounce, uint128 value, uint16 flag) public pure checkOwnerAndAccept {
        // Runtime function that allows to make a transfer with arbitrary settings.
        dest.transfer(value, false, 1);
    }
    function sendTransactionNoFeeNoReturn(address dest, bool bounce, uint128 value, uint16 flag) public pure checkOwnerAndAccept {
        // Runtime function that allows to make a transfer with arbitrary settings.
        dest.transfer(value, false, 0);
    }
    function sendTransactionAll(address dest, bool bounce, uint128 value, uint16 flag) public pure checkOwnerAndAccept {
        // Runtime function that allows to make a transfer with arbitrary settings.
        dest.transfer(value, false, 128);
    }
}