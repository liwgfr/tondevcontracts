pragma ton-solidity >= 0.35.0;
pragma AbiHeader expire;

//2.1. Написать смарт-контракт и задеплоить его локально. Суть контракта:
//
//    контракт должен хранить произведение чисел. Изначально инициализирован единицей.
//    Bметь функцию "умножить".  Функция очевидно должна умножать сохраненное число на переданный в нее параметр.
//    Дополнительным моментом является то, что функция должна умножать только на числа от 1 до 10 включительно.
//    В противном случае прерывать выполнение. Используем require

contract Multiplicator {
    uint public res = 1;

    constructor() public {
        require(tvm.pubkey() != 0, 101);
        require(msg.pubkey() == tvm.pubkey(), 102);
        tvm.accept();
    }
    modifier checkOwnerAndAccept {
        require(msg.pubkey() == tvm.pubkey(), 102);
        tvm.accept();
        _;
    }

    function add(uint value) public checkOwnerAndAccept {
        require(value >= uint(1) && value <= uint(10), 101);
        res *= value;
    }

}